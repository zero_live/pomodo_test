const INITIAL_TIME_LEFT = 25
const MINUTE = 1
const NO_TIME_LEFT = 0

class Pomodoro {
  constructor(bus) {
    this.bus = bus
    this.timeLeft = NO_TIME_LEFT
    this.paused = false

    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.start_requested', this.start.bind(this))
    this.bus.subscribe('timer.askTimeLeft', this.calculateTimeLeft.bind(this))
    this.bus.subscribe('timer.pause', this.pause.bind(this))
    this.bus.subscribe('timer.reset', this.reset.bind(this))

  }

  start() {
    
    if(!this._hasTimeLeft()) { this._initializeTimeLeft() }
    this.paused = false 

    this.bus.publish('timer.start', this._message())
    
  }

  pause() {
    this.paused = true
    this.bus.publish('timer.paused', this._message())
  }

  reset() {
    this.paused = true
    this.timeLeft = INITIAL_TIME_LEFT
    this.bus.publish('timer.reseted', this._message())
  }

  calculateTimeLeft() {  
    if(!this.paused && this._hasTimeLeft()) { this.timeLeft -= MINUTE }
    
    this.bus.publish('timer.timeLeft', this._message())
    
  }

  _initializeTimeLeft() {
    this.timeLeft = INITIAL_TIME_LEFT
  }

  _hasTimeLeft() {
    return (this.timeLeft > NO_TIME_LEFT)
  }
  _message() {
    return { isPaused: this.paused, minutes: this.timeLeft }
  }
}

export default Pomodoro
