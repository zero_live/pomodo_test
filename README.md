# Pomodoro Devscola

This is an project to learn JavaScript in the "Run" from Zero in Devscola.

The project consist in a Pomodoro timer where shows the time left in minutes.

## System requirements

- Firefox version 60 or higher.

## How to run the application

Just open the `index.html` file in a project's main folder with your Firefox.

## How to run the tests

Open the `tests.html` file in a project's main folder with your Firefox ;P

# Exercises

Execute the tests in the next commit and solve it:

- Serve the proyect with:

`python2 -m SimpleHTTPServer 8080`

- Last exercise:

`git checkout d822be2`

- Third exercise:

`git checkout ad6744b`

- Second exercise:

`git checkout 96c4610`

- First exercise:

`git checkout 67edb3c`
